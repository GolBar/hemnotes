//
//  Note.swift
//  HemNotes
//
//  Created by Bartosz Gola on 22/02/2019.
//  Copyright © 2019 Bartosz Gola. All rights reserved.
//

import Foundation
import UIKit

class Note: NSObject, Codable{
    
    var title = ""
    var noteText = ""
    var images = [String]()
    var password = ""
}
