//
//  NoteViewController.swift
//  HemNotes
//
//  Created by Bartosz Gola on 25/02/2019.
//  Copyright © 2019 Bartosz Gola. All rights reserved.
//

import UIKit


class NoteViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, EditImageViewControllerDelegate {
    func backWithoutEditing() {
        navigationController?.popViewController(animated: true)
    }
    
    
    var note: Note!
    var newNote = Note()
    var collectionViewCellModifier: CGFloat = 0
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var textField: UITextView!
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var passwordField: UITextField!
    
    var delegate: ListNotesTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if note != nil{
            loadPassedNoteTexts()
            newNote.title = note.title
            newNote.noteText = note.noteText
            newNote.images = note.images
            newNote.password = note.password
        }
        else{
            prepareTextsForEdit()
        }
        
        passwordField.isSecureTextEntry = true
    }
    
    @IBAction func done(){
        updateNewNoteTexts()
        
        let title = "Saved!"
        let message = "Your note has beed saved"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okOption = UIAlertAction(title: "OK", style: .default, handler: {_ in self.addOrEditNote()})
        
        alert.addAction(okOption)
        
        present(alert, animated: true, completion: nil)
        
        
        
    }
    
    @IBAction func cancel()
    {
        updateNewNoteTexts()
        if didChange(){
            let title = "The changes are not saved"
            let message = "If you continue you will lose changes done"
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let cancelOption = UIAlertAction(title: "Cancel", style: .default)
            let continueOption = UIAlertAction(title: "Continue", style: .default, handler: {_ in self.goBack() })
            
            alert.addAction(continueOption)
            alert.addAction(cancelOption)
            
            present(alert, animated: true, completion: nil)
        }
        else{
            goBack()
        }
    }
    
    func updateNewNoteTexts(){
        newNote.title = titleField.text!
        newNote.noteText = textField.text!
        newNote.password = passwordField.text!
    }
    
    func loadPassedNoteTexts(){
        title = note.title
        titleField.text = note.title
        textField.text = note.noteText
        passwordField.text = note.password
    }
    
    func prepareTextsForEdit(){
        title = "Create new note"
        titleField.text = ""
        textField.text = ""
        passwordField.text = ""
    }
    
    func didChange() -> Bool{
        if note != nil{
            return note.title != newNote.title || note.noteText != newNote.noteText || note.images != newNote.images || note.password != newNote.password
        }
        else{
            return false
        }
    }

    func addOrEditNote(){
        if note == nil{
            MainManager.shared.notes.append(newNote)
            goBack()
        }
        else{
            note.title = newNote.title
            note.noteText = newNote.noteText
            note.images = newNote.images
            note.password = newNote.password
            
            goBack()
        }
    }
    
    func imageFromLibrary(){
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        
        self.present(image, animated: true)
    }
    
    func imageFromCamera(){
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerController.SourceType.camera
        image.allowsEditing = false
        
        self.present(image, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            var data: Data?
            data = UIImage.pngData(image)()
            //let generatedName = data?.base64EncodedString()
            let date = Date()
            let formater = DateFormatter()
            formater.dateFormat = "ddMMyyyyHHmmss"
            let dateString  = formater.string(from: date)
            
            let name = "img"+dateString
            //let name = generatedName! + ".png"
            newNote.images.append(name)

            MainManager.shared.saveImageDocument(name: name, imageData: data!)
//            UserDefaults.standard.set(data, forKey: path!)
//            UserDefaults.standard.synchronize()
            
            collection.reloadData()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateNote(editedNote: Note) {
        newNote.images = editedNote.images
        goBack()
        collection.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditImage"{
            let ctrl = segue.destination as! EditImageViewController
            ctrl.delegate = self
            ctrl.editNote = newNote
            let index = sender as! IndexPath
            ctrl.imageIndex = index.row
        }
    }
    
    func goBack(){
        
        delegate?.reload()
        navigationController?.popViewController(animated: true)
    }
    
    
}


