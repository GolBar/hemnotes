//
//  ListNotesTableViewController.swift
//  HemNotes
//
//  Created by Bartosz Gola on 22/02/2019.
//  Copyright © 2019 Bartosz Gola. All rights reserved.
//

import UIKit
import MessageUI

protocol ListNotesTableViewControllerDelegate{
    func reload()
}

class ListNotesTableViewController: UITableViewController, MFMailComposeViewControllerDelegate, ListNotesTableViewControllerDelegate {

    @IBOutlet var emptyView: UIView!
    
    @IBOutlet weak var button: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        tableView.reloadData()
        button.layer.cornerRadius = 5
        view.addSubview(emptyView)
        emptyView.frame.size = (CGSize(width: view.frame.width, height: view.frame.height))
        loadProperView()
        
    }
    
    func configureText(for cell: UITableViewCell, with item: Note) {
        let title = cell.viewWithTag(1000) as! UILabel
        title.text = item.title
        
        let subtitle = cell.viewWithTag(1001) as! UILabel
        subtitle.text = item.noteText
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MainManager.shared.notes.count
    }
    //slide to delete
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let title = MainManager.shared.notes[indexPath.row].title
        showDeleteAlert(noteTitle: title, indexPath: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoteListItem", for: indexPath)

            let item = MainManager.shared.notes[indexPath.row]
 
            configureText(for: cell, with: item)

            return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if MainManager.shared.notes[indexPath.row].password != ""{
            askForPassword(indexPath: indexPath, mail: false)
        }
        else{
            self.performSegue(withIdentifier: "EditNote", sender: indexPath)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        if MainManager.shared.notes[indexPath.row].password != ""{
            askForPassword(indexPath: indexPath, mail: true)
        }
        else{
            sendMail(indexPath: indexPath)
        }
    }
    //change color of delete button to gray
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteButton = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
            self.tableView.dataSource?.tableView!(self.tableView, commit: .delete, forRowAt: indexPath)
            return
        }
        deleteButton.backgroundColor = UIColor.lightGray
        return [deleteButton]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditNote"{
            let ctrl = segue.destination as! NoteViewController
            ctrl.delegate = self
            if let indexPath = sender as? IndexPath {
                ctrl.note = MainManager.shared.notes[indexPath.row]
            }
            else{
                print("Problem when preparing for \"EditNote\" segue")
            }
        }
        if segue.identifier == "NewNote"{
            let ctrl = segue.destination as! NoteViewController
            ctrl.delegate = self
        }
    }
    
    func showDeleteAlert(noteTitle: String, indexPath: IndexPath){
        let title = "Delete Note"
        let message = "Are you sure you want to delete \"\(noteTitle)\" "
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelOption = UIAlertAction(title: "Cancel", style: .default)
        let yesOption = UIAlertAction(title: "Yes", style: .default, handler: {_ in self.deleteNote(indexPath: indexPath) })
        
        alert.addAction(yesOption)
        alert.addAction(cancelOption)
        
        present(alert, animated: true, completion: nil)
    }
    
    func deleteNote(indexPath: IndexPath){
        
        for i in MainManager.shared.notes[indexPath.row].images{
            MainManager.shared.deleteImageDocument(name: i)
        }
        
        MainManager.shared.notes.remove(at: indexPath.row)
        
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths, with: .automatic)
        loadProperView()
    }
    
    func askForPassword(indexPath: IndexPath, mail: Bool){
        let title = "Password"
        let message = "Enter password for note: \"\(MainManager.shared.notes[indexPath.row].title)\" "
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addTextField{
            
            (textField) in textField.isSecureTextEntry = true;

        }
        let cancelOption = UIAlertAction(title: "Cancel", style: .default)
        var yesOption: UIAlertAction!
        if !mail{
            yesOption = UIAlertAction(title: "Yes", style: .default, handler: {_ in self.performSegueWithPassword(text: alert.textFields![0].text!, indexPath: indexPath)})
        }
        else{
            yesOption = UIAlertAction(title: "Yes", style: .default, handler: {_ in self.sendMailWithPassword(text: alert.textFields![0].text!, indexPath: indexPath)})
        }
        
        
        alert.addAction(yesOption)
        alert.addAction(cancelOption)
        
        present(alert, animated: true, completion: nil)
    }
    
    func sendMailWithPassword(text: String, indexPath: IndexPath){
        if(text == MainManager.shared.notes[indexPath.row].password){
            sendMail(indexPath: indexPath)
        }
    }
    
    
    func performSegueWithPassword(text: String, indexPath: IndexPath){
        if(text == MainManager.shared.notes[indexPath.row].password){
            self.performSegue(withIdentifier: "EditNote", sender: indexPath)
        }
    }
    
    func mailComposeController(_ didFinishWithcontroller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sendMail(indexPath: IndexPath) {
        if(MFMailComposeViewController.canSendMail()){
            print("Can send email.")
            
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self
            
            //Set the subject
            mailComposer.setSubject(MainManager.shared.notes[indexPath.row].title)
            
            //set mail body
            mailComposer.setMessageBody(MainManager.shared.notes[indexPath.row].noteText, isHTML: true)
            
            for i in MainManager.shared.notes[indexPath.row].images{
                let imageData = UIImage.pngData(MainManager.shared.loadImage(name: i)!)()
                
                mailComposer.addAttachmentData(imageData!, mimeType: "application/png", fileName: i)
            }
            
            self.present(mailComposer, animated: true, completion: nil)
        }
        else
        {
            print("email is not supported")
        }
        
        
    }
    @IBAction func createNewNoteButton(_ sender: Any) {
        performSegue(withIdentifier: "NewNote", sender: nil)
    }
    
    func loadProperView(){
        
        if MainManager.shared.notes.count == 0{
            emptyView.isHidden = false
        }
        else{
            emptyView.isHidden = true
        }
    }
    
    func reload() {
        tableView.reloadData()
        loadProperView()
    }
}


