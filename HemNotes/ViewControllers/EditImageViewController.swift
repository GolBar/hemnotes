//
//  EditImageViewController.swift
//  HemNotes
//
//  Created by Bartosz Gola on 26/02/2019.
//  Copyright © 2019 Bartosz Gola. All rights reserved.
//

import UIKit

protocol EditImageViewControllerDelegate:  class{
    func updateNote(editedNote: Note)
    func backWithoutEditing()
}

class EditImageViewController: UIViewController {

    weak var delegate: EditImageViewControllerDelegate!
    var imageIndex: Int!
    var editNote: Note!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var button: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        button.layer.cornerRadius = 5
        imageView.image = MainManager.shared.loadImage(name: editNote.images[imageIndex])
    }
    
    @IBAction func back(_ sender: Any) {
        delegate.backWithoutEditing()
    }
    
    @IBAction func deletePicture(_ sender: Any) {
        
        editNote.images.remove(at: imageIndex)
        delegate.updateNote(editedNote: editNote)
    }
    
}
