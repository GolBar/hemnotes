//
//  EmptyNotesViewController.swift
//  HemNotes
//
//  Created by Bartosz Gola on 22/02/2019.
//  Copyright © 2019 Bartosz Gola. All rights reserved.
//

import UIKit

class EmptyNotesViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        button.layer.cornerRadius = 5
        
    }

    @IBAction func moveToCreateNoteView(){
        performSegue(withIdentifier: "NewNote", sender: nil)
    }

}
