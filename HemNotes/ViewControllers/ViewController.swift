//
//  ViewController.swift
//  HemNotes
//
//  Created by Bartosz Gola on 22/02/2019.
//  Copyright © 2019 Bartosz Gola. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private let logoTimer: UInt32 = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        MainManager.shared.createPhotoDirectory()
        MainManager.shared.loadData()
        sleep(logoTimer)
        performSegue(withIdentifier: "NotesList", sender: nil)
    }
}

