//
//  NoteViewController+CollectionView.swift
//  HemNotes
//
//  Created by Bartosz Gola on 11/03/2019.
//  Copyright © 2019 Bartosz Gola. All rights reserved.
//

import Foundation
import UIKit

extension NoteViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if newNote.images.count == 6{
            return 6
        }
        else{
            return newNote.images.count + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
        if !(newNote.images.count < 6 && indexPath.row == newNote.images.count) {
            
            if MainManager.shared.loadImage(name: newNote.images[indexPath.row]) != nil{
                cell.imageView.image = MainManager.shared.loadImage(name: newNote.images[indexPath.row])
            }
            //            let myData = UserDefaults.standard.data(forKey: newNote.images[indexPath.row])
            //            cell.imageView.image = UIImage(data: myData!)
            
        }else{
            cell.imageView.image = UIImage(named: "ButtonPicture")
        }
        
        cell.contentView.layer.cornerRadius = collectionViewCellModifier/2
        cell.contentView.layer.masksToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if newNote.images.count < 6 && indexPath.row == newNote.images.count {
            
            let action = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
            let cancelOption = UIAlertAction(title: "Cancel", style: .default)
            let libraryOption = UIAlertAction(title: "Choose From Library", style: .default, handler: {_ in self.imageFromLibrary() })
            let cameraOption = UIAlertAction(title: "Take Photo", style: .default, handler: {_ in self.imageFromCamera() })
            
            action.addAction(libraryOption)
            action.addAction(cameraOption)
            action.addAction(cancelOption)
            
            present(action, animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: "EditImage", sender: indexPath)
        }
    }
    
    //colection view design
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        collectionViewCellModifier = collectionView.bounds  .width/4.0
        let yourWidth = collectionViewCellModifier
        let yourHeight = yourWidth
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return collectionViewCellModifier/2.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return collectionViewCellModifier/2.0
    }
}
