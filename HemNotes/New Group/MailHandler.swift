//
//  MailHandler.swift
//  HemNotes
//
//  Created by Bartosz Gola on 11/03/2019.
//  Copyright © 2019 Bartosz Gola. All rights reserved.
//

import MessageUI

class MailHandler: MFMailComposeViewControllerDelegate{
    func mailComposeController(_ didFinishWithcontroller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sendMail(indexPath: IndexPath) {
        if(MFMailComposeViewController.canSendMail()){
            print("Can send email.")
            
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self
            
            //Set the subject
            mailComposer.setSubject(MainManager.shared.notes[indexPath.row].title)
            
            //set mail body
            mailComposer.setMessageBody(MainManager.shared.notes[indexPath.row].noteText, isHTML: true)
            
            for i in MainManager.shared.notes[indexPath.row].images{
                let imageData = UIImage.pngData(MainManager.shared.loadImage(name: i)!)()
                
                mailComposer.addAttachmentData(imageData!, mimeType: "application/png", fileName: i)
            }
            
            self.present(mailComposer, animated: true, completion: nil)
        }
        else
        {
            print("email is not supported")
        }
        
        
    }
}
