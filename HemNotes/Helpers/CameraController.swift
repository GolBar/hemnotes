//
//  CameraController.swift
//  HemNotes
//
//  Created by Bartosz Gola on 11/03/2019.
//  Copyright © 2019 Bartosz Gola. All rights reserved.
//

import Foundation
import UIKit

class CameraController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imageFromLibrary(_ ){
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        
        self.present(image, animated: true)
    }
    
    func imageFromCamera(){
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerController.SourceType.camera
        image.allowsEditing = false
        
        self.present(image, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            var data: Data?
            data = UIImage.pngData(image)()
            //let generatedName = data?.base64EncodedString()
            let date = Date()
            let formater = DateFormatter()
            formater.dateFormat = "ddMMyyyyHHmmss"
            let dateString  = formater.string(from: date)
            
            let name = "img"+dateString
            //let name = generatedName! + ".png"
            newNote.images.append(name)
            
            MainManager.shared.saveImageDocument(name: name, imageData: data!)
            //            UserDefaults.standard.set(data, forKey: path!)
            //            UserDefaults.standard.synchronize()
            
            collection.reloadData()
        }
        self.dismiss(animated: true, completion: nil)
    }
}

