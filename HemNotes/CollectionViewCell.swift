//
//  CollectionViewCell.swift
//  HemNotes
//
//  Created by Bartosz Gola on 26/02/2019.
//  Copyright © 2019 Bartosz Gola. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    

    @IBOutlet weak var imageView: UIImageView!
    
}
