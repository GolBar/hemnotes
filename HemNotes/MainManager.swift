//
//  MainManager.swift
//  HemNotes
//
//  Created by Bartosz Gola on 22/02/2019.
//  Copyright © 2019 Bartosz Gola. All rights reserved.
//

import Foundation
import UIKit

class MainManager{
    
    //singleton
    static let shared = MainManager()
    private init() {}
    /////////////////////////////////
    
    var notes = [Note]()
    
    func createPhotoDirectory(){
        let paths = photoDirectory()
        let fileManager = FileManager.default;
        if !fileManager.fileExists(atPath: paths.path){
            try! fileManager.createDirectory(at: paths, withIntermediateDirectories: true, attributes: nil)
        }else{
            print("Already created.")
        }
    }
    
    func saveImageDocument(name: String, imageData: Data){
        let fileManager = FileManager.default
        let paths = photoDirectory().appendingPathComponent(name)
        fileManager.createFile(atPath: paths.path, contents: imageData, attributes: nil)
    }
    
    func deleteImageDocument(name: String){
        let fileManager = FileManager.default
        let paths = photoDirectory().appendingPathComponent(name)
        do{
            try fileManager.removeItem(atPath: paths.path)
        }
        catch{
            print("Failed to delete image: " + name)
        }
    }
    
    func loadImage(name: String) -> UIImage?{
        let fileManager = FileManager.default
        let imagePath = photoDirectory().appendingPathComponent(name)
        if fileManager.fileExists(atPath: imagePath.path){
            return UIImage(contentsOfFile: imagePath.path)
        }else{
            return nil
        }
    }
    
    func documentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func photoDirectory() -> URL {
        
        return documentsDirectory().appendingPathComponent("PhotoFolder")
        
    }
    
    func saveDataFilePath() -> URL {
        return documentsDirectory().appendingPathComponent(
            "HemNotes.plist")
    }
    
    func saveData() {
        let encoder = PropertyListEncoder()
        do {
            let data = try encoder.encode(notes)
            try data.write(to: saveDataFilePath(), options: Data.WritingOptions.atomic)
        } catch {
            print("Error encoding item array: \(error.localizedDescription)")
        }
    }
    
    func loadData() {
        let path = saveDataFilePath()
        if let data = try? Data(contentsOf: path) {
            let decoder = PropertyListDecoder()
            do {
                notes = try decoder.decode([Note].self, from: data)
            } catch {
                print("Error decoding list array: \(error.localizedDescription)")
            }
        }
    }
}
